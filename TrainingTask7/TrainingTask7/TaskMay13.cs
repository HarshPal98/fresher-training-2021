﻿using System;

namespace TrainingTask7
{
    public class Party
    {
        public string partyVenue, bookingtype;
        public int noOfPeople;
        public double decorationCharges, cateringCharges;

        public virtual void PartyPackage(string partyvenue)
        {

        }

        public virtual void DisplayPartyDetails(string packageName, double decorationCharges, double cateringCharges)
        {

        }
    }

    class Birthday : Party
    {
        int readVenue, readPackage, bookingId;
        string bookingDate;
        public void BirthdayOrganizer(int totalPeople, string date, int value)
        {
            bookingDate = date;
            bookingId = value;
            base.noOfPeople = totalPeople;
            try
            {
                Console.WriteLine("Welcome to Birthday Party Bookings");
                Console.WriteLine("The list of Venues are given below:");
                Console.WriteLine("1. Radisson Blue\n2.Chattarpur Mini Farms");
                Console.WriteLine("Enter your choice");
                readVenue = Convert.ToInt32(Console.ReadLine());
            }
            catch(Exception e)
            {
                Console.WriteLine(e);
            }
            
            switch (readVenue)
            {
                case 1:
                    base.partyVenue = "Radisson Blue";
                    PartyPackage(base.partyVenue);
                    break;
                case 2:
                    base.partyVenue = "Chattarpur Mini Farms";
                    PartyPackage(base.partyVenue);
                    break;
                default:
                    Console.WriteLine("Wrong Choice");
                    break;
            }
        }

        public override void PartyPackage(string partyVenue)
        {
            try
            {
                Console.WriteLine("Select the Package");
                Console.WriteLine("1. Silver Pacakge");
                Console.WriteLine("2. Gold Package");
                Console.WriteLine("3. Platinum Package");
                readPackage = Convert.ToInt32(Console.ReadLine());
            }
            catch(Exception e)
            {
                Console.WriteLine(e);
            }

            switch (readPackage)
            {
                case 1:
                    SilverPackage();
                    break;
                case 2:
                    GoldPackage();
                    break;
                case 3:
                    PlatinumPackage();
                    break;
                default:
                    Console.WriteLine("Wrong Choice");
                    break;
            }
        }

        void SilverPackage()
        {
            string packageSilver = "Silver Package";
            base.decorationCharges = 12000;
            base.cateringCharges = 25000;
            DisplayPartyDetails(packageSilver, base.decorationCharges, base.cateringCharges);
        }

        void GoldPackage()
        {
            string packageSilver = "Gold Package";
            base.decorationCharges = 15000;
            base.cateringCharges = 30000;
            DisplayPartyDetails(packageSilver, base.decorationCharges, base.cateringCharges);
        }

        void PlatinumPackage()
        {
            string packageSilver = "Platinum Package";
            base.decorationCharges = 17000;
            base.cateringCharges = 35000;
            DisplayPartyDetails(packageSilver, base.decorationCharges, base.cateringCharges);
        }

        public override void DisplayPartyDetails(string packageName, double decorationCharges, double cateringCharges)
        {
            if (packageName == "Silver Package" && noOfPeople <= 150)
            {
                Console.WriteLine("--Booking Confirmed--");
                Console.WriteLine("Partey Type :- Birthday Party");
                Console.WriteLine("Booking ID :- " + bookingId);
                Console.WriteLine("Pacakge :- " + packageName);
                Console.WriteLine("Venue :- " + base.partyVenue);
                Console.WriteLine("Booking Date :- " + bookingDate);
                double totalCost = decorationCharges + cateringCharges;
                Console.WriteLine("Cost Per Person :- " + totalCost / base.noOfPeople);
                Console.WriteLine("Package Includes-");
                Console.WriteLine("Includes Unlimited Buffet....Maximum 150 people allowed");
                Console.WriteLine();
            }
            else if (packageName == "Gold Package" && noOfPeople > 150 && noOfPeople <= 250)
            {
                Console.WriteLine("--Booking Confirmed--");
                Console.WriteLine("Partey Type :- Birthday Party");
                Console.WriteLine("Booking ID :- " + bookingId);
                Console.WriteLine("Pacakge :- " + packageName);
                Console.WriteLine("Venue :- " + base.partyVenue);
                Console.WriteLine("Booking Date :- " + bookingDate);
                double totalCost = decorationCharges + cateringCharges;
                Console.WriteLine("Cost Per Person :- " + totalCost / base.noOfPeople);
                Console.WriteLine("Package Includes-");
                Console.WriteLine("Includes Unlimited Buffet+Mocktails....Maximum 250 people allowed");
                Console.WriteLine();
            }
            else
            {
                Console.WriteLine("--Booking Confirmed--");
                Console.WriteLine("Partey Type :- Birthday Party");
                Console.WriteLine("Booking ID :- " + bookingId);
                Console.WriteLine("Pacakge :- " + packageName);
                Console.WriteLine("Venue :- " + base.partyVenue);
                Console.WriteLine("Booking Date :- " + bookingDate);
                double totalCost = decorationCharges + cateringCharges;
                Console.WriteLine("Cost Per Person :- " + totalCost / base.noOfPeople);
                Console.WriteLine("Package Includes-");
                Console.WriteLine("Includes Unlimited Buffet+Moktails+DJ....Maximum 350 people allowed");
                Console.WriteLine();
            }
        }
    }

    class Anniversary : Party
    {
        int readVenue, readPacakge, bookingId;
        string bookingDate;
        public void AnniversaryOrganizer(int totalPeople, string date, int value)
        {
            bookingDate = date;
            bookingId = value;
            base.noOfPeople = totalPeople;
            try
            {
                Console.WriteLine("Welcome to Anniversary Party Bookings");
                Console.WriteLine("The list of Venues are given below:");
                Console.WriteLine("1. Radisson Blue\n2.Chattarpur Mini Farms");
                Console.WriteLine("Enter your choice");
                readVenue = Convert.ToInt32(Console.ReadLine());
            }
            catch(Exception e)
            {
                Console.WriteLine(e);
            }

            switch (readVenue)
            {
                case 1:
                    base.partyVenue = "Radisson Blue";
                    PartyPackage(base.partyVenue);
                    break;
                case 2:
                    base.partyVenue = "Chattarpur Mini Farms";
                    PartyPackage(base.partyVenue);
                    break;
                default:
                    Console.WriteLine("Wrong Choice");
                    break;
            }
        }

        public override void PartyPackage(string partyVenue)
        {
            try
            {
                Console.WriteLine("Select the Package");
                Console.WriteLine("1. Silver Pacakge");
                Console.WriteLine("2. Gold Package");
                Console.WriteLine("3. Platinum Package");
                readPacakge = Convert.ToInt32(Console.ReadLine());
            }
            catch(Exception e)
            {
                Console.WriteLine(e);
            }

            switch (readPacakge)
            {
                    case 1: SilverPackage();
                        break;
                    case 2: GoldPackage();
                        break;
                    case 3: PlatinumPackage();
                        break;
                    default: Console.WriteLine("Wrong Choice");
                        break;
            }
        }

        void SilverPackage()
        {
            string packageSilver = "Silver Package";
            base.decorationCharges = 10000;
            base.cateringCharges = 20000;
            DisplayPartyDetails(packageSilver, base.decorationCharges, base.cateringCharges);
        }

        void GoldPackage()
        {
            string packageSilver = "Gold Package";
            base.decorationCharges = 12000;
            base.cateringCharges = 25000;
            DisplayPartyDetails(packageSilver, base.decorationCharges, base.cateringCharges);
        }

        void PlatinumPackage()
        {
            string packageSilver = "Platinum Package";
            base.decorationCharges = 15000;
            base.cateringCharges = 30000;
            DisplayPartyDetails(packageSilver, base.decorationCharges, base.cateringCharges);
        }

        public override void DisplayPartyDetails(string packageName, double decorationCharges, double cateringCharges)
        {
            if (packageName == "Silver Package" && noOfPeople <= 150)
            {
                Console.WriteLine("--Booking Confirmed--");
                Console.WriteLine("Partey Type :- Anniversary Party");
                Console.WriteLine("Booking ID :- " + bookingId);
                Console.WriteLine("Pacakge :- " + packageName);
                Console.WriteLine("Venue :- " + base.partyVenue);
                Console.WriteLine("Booking Date :- " + bookingDate);
                double totalCost = decorationCharges + cateringCharges;
                Console.WriteLine("Cost Per Person :- " + totalCost / base.noOfPeople);
                Console.WriteLine("Package Includes-");
                Console.WriteLine("Includes Unlimited Buffet....Maximum 150 people allowed");
                Console.WriteLine();
            }
            else if (packageName == "Gold Package" && noOfPeople > 150 && noOfPeople <= 250)
            {
                Console.WriteLine("--Booking Confirmed--");
                Console.WriteLine("Partey Type :- Anniversary Party");
                Console.WriteLine("Booking ID :- " + bookingId);
                Console.WriteLine("Pacakge :- " + packageName);
                Console.WriteLine("Venue :- " + base.partyVenue);
                Console.WriteLine("Booking Date :- " + bookingDate);
                double totalCost = decorationCharges + cateringCharges;
                Console.WriteLine("Cost Per Person :- " + totalCost / base.noOfPeople);
                Console.WriteLine("Package Includes-");
                Console.WriteLine("Includes Unlimited Buffet+Mocktails....Maximum 250 people allowed");
                Console.WriteLine();
            }
            else
            {
                Console.WriteLine("--Booking Confirmed--");
                Console.WriteLine("Partey Type :- Anniversary Party");
                Console.WriteLine("Booking ID :- " + bookingId);
                Console.WriteLine("Pacakge :- " + packageName);
                Console.WriteLine("Venue :- " + base.partyVenue);
                Console.WriteLine("Booking Date :- " + bookingDate);
                double totalCost = decorationCharges + cateringCharges;
                Console.WriteLine("Cost Per Person :- " + totalCost / base.noOfPeople);
                Console.WriteLine("Package Includes-");
                Console.WriteLine("Includes Unlimited Buffet+Moktails+DJ....Maximum 350 people allowed");
                Console.WriteLine();
            }
        }
    }

    class Retirement : Party
    {

        int readVenue, readPackage, bookingId;
        string bookingDate;
        public void RetirementOrganizer(int totalPeople, string date, int value)
        {
            bookingDate = date;
            bookingId = value;
            base.noOfPeople = totalPeople;
            
            try
            {
                Console.WriteLine("Welcome to Birthday Party Bookings");
                Console.WriteLine("The list of Venues are given below:");
                Console.WriteLine("1. Radisson Blue\n2.Chattarpur Mini Farms");
                Console.WriteLine("Enter your choice");
                readVenue = Convert.ToInt32(Console.ReadLine());
            }
            catch(Exception e)
            {
                Console.WriteLine(e);
            }
            finally
            {
                Console.WriteLine("Please try again");
            }
            
            switch (readVenue)
            {
                case 1:
                    base.partyVenue = "Radisson Blue";
                    PartyPackage(base.partyVenue);
                    break;
                case 2:
                    base.partyVenue = "Chattarpur Mini Farms";
                    PartyPackage(base.partyVenue);
                    break;
                default:
                    Console.WriteLine("Wrong Choice");
                    break;
            }
        }

        public override void PartyPackage(string partyVenue)
        {
            try
            {
                Console.WriteLine("Select the Package");
                Console.WriteLine("1. Silver Pacakge");
                Console.WriteLine("2. Gold Package");
                Console.WriteLine("3. Platinum Package");
                readPackage = Convert.ToInt32(Console.ReadLine());
            }
            catch(Exception e)
            {
                Console.WriteLine(e);
            }
            

            switch (readPackage)
            {
                case 1:
                    SilverPackage();
                    break;
                case 2:
                    GoldPackage();
                    break;
                case 3:
                    PlatinumPackage();
                    break;
                default:
                    Console.WriteLine("Wrong Choice");
                    break;
            }
        }

        void SilverPackage()
        {
            string packageSilver = "Silver Package";
            base.decorationCharges = 8000;
            base.cateringCharges = 10000;
            DisplayPartyDetails(packageSilver, base.decorationCharges, base.cateringCharges);
        }

        void GoldPackage()
        {
            string packageSilver = "Gold Package";
            base.decorationCharges = 12000;
            base.cateringCharges = 15000;
            DisplayPartyDetails(packageSilver, base.decorationCharges, base.cateringCharges);
        }

        void PlatinumPackage()
        {
            string packageSilver = "Platinum Package";
            base.decorationCharges = 15000;
            base.cateringCharges = 20000;
            DisplayPartyDetails(packageSilver, base.decorationCharges, base.cateringCharges);
        }

        public override void DisplayPartyDetails(string packageName, double decorationCharges, double cateringCharges)
        {
            if (packageName == "Silver Package" && noOfPeople <= 150)
            {
                Console.WriteLine("--Booking Confirmed--");
                Console.WriteLine("Partey Type :- Retirement Party");
                Console.WriteLine("Booking ID :- " + bookingId);
                Console.WriteLine("Pacakge :- " + packageName);
                Console.WriteLine("Venue :- " + base.partyVenue);
                Console.WriteLine("Booking Date :- " + bookingDate);
                double totalCost = decorationCharges + cateringCharges;
                Console.WriteLine("Cost Per Person :- " + totalCost / base.noOfPeople);
                Console.WriteLine("Package Includes-");
                Console.WriteLine("Includes Unlimited Buffet....Maximum 150 people allowed");
                Console.WriteLine();
            }
            else if (packageName == "Gold Package" && noOfPeople > 150 && noOfPeople <= 250)
            {
                Console.WriteLine("--Booking Confirmed--");
                Console.WriteLine("Partey Type :- Retirement Party");
                Console.WriteLine("Booking ID :- " + bookingId);
                Console.WriteLine("Pacakge :- " + packageName);
                Console.WriteLine("Venue :- " + base.partyVenue);
                Console.WriteLine("Booking Date :- " + bookingDate);
                double totalCost = decorationCharges + cateringCharges;
                Console.WriteLine("Cost Per Person :- " + totalCost / base.noOfPeople);
                Console.WriteLine("Package Includes-");
                Console.WriteLine("Includes Unlimited Buffet+Mocktails....Maximum 250 people allowed");
                Console.WriteLine();
            }
            else
            {
                Console.WriteLine("--Booking Confirmed--");
                Console.WriteLine("Partey Type :- Retirement Party");
                Console.WriteLine("Booking ID :- " + bookingId);
                Console.WriteLine("Pacakge :- " + packageName);
                Console.WriteLine("Venue :- " + base.partyVenue);
                Console.WriteLine("Booking Date :- " + bookingDate);
                double totalCost = decorationCharges + cateringCharges;
                Console.WriteLine("Cost Per Person :- " + totalCost / base.noOfPeople);
                Console.WriteLine("Package Includes-");
                Console.WriteLine("Includes Unlimited Buffet+Moktails+DJ....Maximum 350 people allowed");
                Console.WriteLine();
            }
        }
    }

    class MainClass
    {
        public static void Main(string[] args)
        {
            //string condition;
            DateTime now = DateTime.Now;
            Random rnd = new Random();
            int value;
            string currentDate = now.ToString("f");
            int bookingType=0;
            try
            {
                Console.WriteLine("Welcome to Party Booking System");
                Console.WriteLine("Select your Booking type");
                Console.WriteLine("1. Birthday Party");
                Console.WriteLine("2. Anniversary Party");
                Console.WriteLine("3.Retirement Party");
                bookingType = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            finally
            {
                Console.WriteLine("Please try again");
                Birthday b = new Birthday();
                Anniversary a = new Anniversary();
                Retirement r = new Retirement();
                int noOfPeople;
                switch (bookingType)
                {
                    case 1:
                        Console.WriteLine("Enter the number of attendies");
                        noOfPeople = Convert.ToInt32(Console.ReadLine());
                        value = rnd.Next(1000000, 9999999);
                        b.BirthdayOrganizer(noOfPeople, currentDate, value);
                        break;
                    case 2:
                        Console.WriteLine("Enter the number of attendies");
                        noOfPeople = Convert.ToInt32(Console.ReadLine());
                        value = rnd.Next(1000000, 9999999);
                        a.AnniversaryOrganizer(noOfPeople, currentDate, value);
                        break;
                    case 3:
                        Console.WriteLine("Enter the number of attendies");
                        noOfPeople = Convert.ToInt32(Console.ReadLine());
                        value = rnd.Next(1000000, 9999999);
                        r.RetirementOrganizer(noOfPeople, currentDate, value);
                        break;
                    default:
                        Console.WriteLine("Wrong Entry");
                        break;
                }

            }
        }
    }
}

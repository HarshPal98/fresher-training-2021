﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqMethods
{
    class MainClass
    {
        public class Student
        {
            public int StudentID { get; set; }
            public String StudentName { get; set; }
            public int Age { get; set; }
            public int Marks { get; set; }
            public int StandardID { get; set; }
        }

        public class Standard
        {
            public int StandardID { get; set; }
            public string StandardName { get; set; }
        }

        public static void Main(string[] args)
        {
            List<Student> std = new List<Student>() {
            new Student(){StudentID = 1, StudentName = "John", Age = 18, Marks = 48, StandardID = 1},
            new Student(){StudentID = 2, StudentName = "Steve",  Age = 21, Marks = 56, StandardID = 1 },
            new Student(){StudentID = 3, StudentName = "Bill",  Age = 25, Marks = 69,StandardID = 2},
            new Student(){StudentID = 4, StudentName = "Ram" , Age = 20, Marks = 76, StandardID = 2},
            new Student(){StudentID = 5, StudentName = "Ron" , Age = 31, Marks = 83, StandardID = 1},
            new Student(){StudentID = 6, StudentName = "Chris",  Age = 17, Marks = 20, StandardID = 2},
            new Student(){StudentID = 7, StudentName = "Rob",Age = 19, Marks = 96, StandardID = 3},
            };

            IList<Student> stdent = new List<Student>() {
            new Student(){StudentID = 1, StudentName = "John", Age = 18, Marks = 48, StandardID = 1 },
            new Student(){StudentID = 2, StudentName = "Steve",  Age = 21, Marks = 56, StandardID = 1 },
            new Student(){StudentID = 3, StudentName = "Bill",  Age = 25, Marks = 69,StandardID = 2},
            new Student(){StudentID = 4, StudentName = "Ram" , Age = 20, Marks = 76,StandardID = 2},
            new Student(){StudentID = 5, StudentName = "Ron" , Age = 31, Marks = 83, StandardID = 1},
            new Student(){StudentID = 6, StudentName = "Chris",  Age = 17, Marks = 20, StandardID = 2},
            new Student(){StudentID = 7, StudentName = "Rob",Age = 19, Marks = 96, StandardID = 3},
            };


            List<Standard> stdList = new List<Standard>() {
            new Standard(){ StandardID = 1, StandardName="Standard 1"},
            new Standard(){ StandardID = 2, StandardName="Standard 2"},
            new Standard(){ StandardID = 3, StandardName="Standard 3"}
            };

            var myAnsQuery = from p in std where p.Marks > 50 select p.StudentName; //where query

            
            foreach (var name in myAnsQuery)
                Console.Write(name + " " + "\n");

            Console.WriteLine("------------");

            var numbers = std.Count(p => p.Age < 25);//count query
            Console.WriteLine(numbers);
            Console.WriteLine("--------------");

            var orderByDescendingResult = from s in std orderby s.StudentName descending select s;//order by descending query
            foreach (var st in orderByDescendingResult)
                Console.WriteLine(st.StudentName);
            Console.WriteLine("--------------");

            var marks = from p in std select p.Marks;
            foreach (var m in marks)
                Console.WriteLine(m + " " + "\n");

            Console.WriteLine("--------------");
            //int avgmarks = (int)Marks.Average();
            //Console.WriteLine(avgmarks);

            var avgmarks = std.Average(x => x.Marks);//average methods
            Console.WriteLine(avgmarks);
            Console.WriteLine("-----------------");

            var thenBY = std.OrderBy(s => s.StudentName).ThenBy(s => s.Marks);//then BY query
            foreach (var st in thenBY)
                Console.WriteLine(st.StudentName+" "+st.Marks);
            Console.WriteLine("--------------");

            var firstjoininner = std.Join(stdList,
                                          student => student.StudentID,
                                          standard => standard.StandardID,
                                          (student, standard) => new
                                          {
                                              StudentName = student.StudentName,
                                              StandardName = standard.StandardName
                                          });
            foreach (var obj in firstjoininner)
            {

                Console.WriteLine("{0} - {1}", obj.StudentName, obj.StandardName);
            }
            Console.WriteLine("----------------");

            var namelist = myAnsQuery.Aggregate((a, b) => a + " ," + b);
            Console.WriteLine(namelist);

            bool flag = std.All(p => p.Age > 30);
            Console.WriteLine(flag);
            Console.WriteLine("----------------");


        }
    }
}

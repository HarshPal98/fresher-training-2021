﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace June31Task
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddApiVersioning(x =>
            {
                x.DefaultApiVersion = new ApiVersion(2, 0);//DefaultApiVersion is used to set the default version to API
                x.AssumeDefaultVersionWhenUnspecified = true;// see comment on next line
// This flag AssumeDefaultVersionWhenUnspecified flag is used to set the default version when the client has not specified any versions.If we haven't set this flag to true and client hit the API without mentioning the version then UnsupportedApiVersion exception occurs.
                x.ReportApiVersions = true;//To return the API version in response header.

            });

// Now from here we look at an example of HTTP header based versioning
          //  services.AddApiVersioning(x =>
          //  {
          //      x.DefaultApiVersion = new ApiVersion(1, 0);
          //      x.AssumeDefaultVersionWhenUnspecified = true;
          //      x.ReportApiVersions = true;
          //      x.ApiVersionReader = new HeaderApiVersionReader("x-api-version");
          //  });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;


namespace June31Task.Controllers
{
    public class EmployeController
    {
        private IEmployeeTest empTest = new EmployeeTest();
        public IEnumerable<Employee> Get()
        {
            return empTest.GetEmployes();
        }
    }
}

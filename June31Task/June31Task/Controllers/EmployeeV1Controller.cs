﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace June31Task.Controllers
{
    [ApiController]
    [ApiVersion("1.0")]
    //[Route("api/employee")]
    [Route("api/{v:apiVersion}/employee")]
    public class EmployeeV1Controller : ControllerBase
    {
        [HttpGet]
        public IActionResult Get()
        {
            return new OkObjectResult("employees from v1 controller");
        }
    }
}
//To call the this version controller we use this URL "https://localhost:5001/api/employee?api-version=1.0" if we don't use line 13
//To call this version controller we use this UR: "https://localhost:5001/api/1.0/employee" in case we use ln 13 instead of 12
﻿using System;
using System.Collections.Generic;

namespace TrainingTask6
{
    public enum Months
    { 
        January=1,
        February=2,
        March=3,
        April=4,
        May=5,
        June=6,
        July=7,
        August=8,
        September=9,
        October=10,
        November=11,
        December=12
    }
    class MainClass
    {
        public static void Main(string[] args)
        {
            List<Person> list = new List<Person>();
            list.Add(new Person("Tom", 32));
            list.Add(new Person("Bob", 40));
            list.Add(new Person("Scott", 25));
            list.Add(new Person("Mikael", 60));
            list.Add(new Person("Rachel", 70));

            foreach (var item in list)
            {
                if (item.getAge > 60)
                {
                    Console.WriteLine(item.getName);
                }
            }

            Dictionary<Months, int> dict = new Dictionary<Months, int>();
            dict.Add(Months.January, 31);
            dict.Add(Months.February, 28);
            dict.Add(Months.March, 31);
            dict.Add(Months.April, 30);
            dict.Add(Months.May, 31);
            dict.Add(Months.June, 30);
            dict.Add(Months.July, 31);
            dict.Add(Months.August, 31);
            dict.Add(Months.September, 30);
            dict.Add(Months.October, 31);
            dict.Add(Months.November, 30);
            dict.Add(Months.December, 31);
            Console.WriteLine("Enter the months name");
            String mon;
            mon = Console.ReadLine();

            retday(dict, mon);
            Console.ReadKey();
        }

        public static void retday(Dictionary<Months, int> dict, String m)
        {
            foreach(var item in dict)
            {
                //Console.WriteLine(item.Value);
                if(String.Equals(item.Key,m))
                {
                    Console.WriteLine(item.Value);
                }
            }
        }
    }

    class Person
    {
        private string name;
        private int age;

        public Person(string name, int age)
        {
            this.name = name;
            this.age = age;
        }

        public String getName { get { return name; } }
        public int getAge { get { return age; } }
    }
}

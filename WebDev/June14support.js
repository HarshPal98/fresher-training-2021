﻿
function validation() {

    var firstname = document.getElementById('frstnm').value;
    var lastname = document.getElementById('lstnm').value;
    var email = document.getElementById('emails').value;
    var password = document.getElementById('exampleInputPassword1').value;
    var cpassword = document.getElementById('exampleInputPassword2').value;
    var mobile = document.getElementById('mobilenumber').value;
    var address = document.getElementById('exampleFormControlTextarea1').value;



    var letters = /^[A-Za-z]+$/;
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var add = /^[a-zA-Z0-9\s,. '-]{3,}$/;
    var mob = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;


    if (firstname == '') {
        document.getElementById('fn').innerHTML = "Please enter your firstname";
        alert('Please enter your firstname');
    }
    else if (!letters.test(firstname)) {
        document.getElementById('fn').innerHTML = "FirstName field required----use only alphabet characters";
        alert('FirstName field required----use only alphabet characters');
    }
    else if (lastname == '') {
        document.getElementById('ln').innerHTML = "Please enter your lastname";
        alert('Please enter your lastname');
    }
    else if (!letters.test(lastname)) {
        document.getElementById('fn').innerHTML = "LastName field required----use only alphabet characters";
        alert('LastName field required----use only alphabet characters');
    }
    else if (email == '') {
        document.getElementById('em').innerHTML = "Please enter an emailId";
        alert('Please enter emailId');
    }
    else if (!filter.test(email)) {
        document.getElementById('em').innerHTML = "Invlaid email";
        alert('Invalid email');
    }
    else if (password == '') {
        document.getElementById('pwd').innerHTML = "Please choose any Password";
        alert('Please choose any Password');
    }
    else if (cpassword == '') {
        document.getElementById('cpwd').innerHTML = "Please Confirm Your Password";
        alert('Please Confirm Your Password');
    }
    else if (!pwd_expression.test(password)) {
        document.getElementById('pwd').innerHTML = "Use uppercase, lowercase, special characters and numbers in this field";
        alert('Upper case, Lower case, Special character and Numeric letter are required in Password filed');
    }
    else if (password != cpassword) {
        document.getElementById('cpwd').innerHTML = "Password not matched";
        alert('Password not Matched');
    }
    else if (mobile == '') {
        document.getElementById('mb').innerHTML = "Please enter mobile number";
        alert('Please enter mobile number');
    }
    else if (!mob.test(mobile)) {
        document.getElementById('mb').innerHTML = "follow the pattern 123-456-7891";
        alert('follow the pattern 123-456-7891');
    }
    else if (address == '') {
        document.getElementById('add').innerHTML = "Please enter your address";
        alert('Please enter your address');
    }
    else if (!add.test(address)) {
        document.getElementById('add').innerHTML = "Enter complete address";
        alert('Enter your full address');
    }
    else if (document.getElementById('exampleInputPassword1').value < 6) {
        document.getElementById('pwd').innerHTML = "Password minimum length is 6";
        alert('Password minimum length is 6');
    }
    else if (document.getElementById('exampleInputPassword1').value > 12) {
        document.getElementById('pwd').innerHTML = "Max length reached";
        alert('Password maximum length is 12');
    }

    else {
        alert('Submission Done!!!');
        Console.Log('Successful');
    }

}

function myFunction() {
    var inpObj = document.getElementById("ex3");
    if (!inpObj.checkValidity()) {
        document.getElementById("demo").innerHTML = inpObj.validationMessage;
    }
}
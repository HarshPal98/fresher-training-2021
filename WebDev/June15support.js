function validation() {

    var firstname = document.getElementById('frstnm').value;
    var lastname = document.getElementById('lstnm').value;
    var email = document.getElementById('emails').value;
    var password = document.getElementById('exampleInputPassword1').value;
    var cpassword = document.getElementById('exampleInputPassword2').value;
    var mobile = document.getElementById('mobilenumber').value;
    var address = document.getElementById('exampleFormControlTextarea1').value;



    var passexpr = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-])/;
    var letters = /^[A-Za-z]+$/;
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var add = /^[a-zA-Z0-9\s,. '-]{3,}$/;
    var mob = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;


    if (firstname == '') {
        document.getElementById('fn').innerHTML = "Please enter your firstname";
        //alert('Please enter your firstname');
        return false;
    }
    else if (!letters.test(firstname)) {
        document.getElementById('fn').innerHTML = "FirstName field required----use only alphabet characters";
        //alert('FirstName field required----use only alphabet characters');
        return false;
    }
    else if (lastname == '') {
        document.getElementById('ln').innerHTML = "Please enter your lastname";
        return false;
    }
    else if (!letters.test(lastname)) {
        document.getElementById('fn').innerHTML = "LastName field required----use only alphabet characters";
        return false;
    }
    else if (email == '') {
        document.getElementById('em').innerHTML = "Please enter an emailId";
        return false;
    }
    else if (!filter.test(email)) {
        document.getElementById('em').innerHTML = "Invlaid email";
        return false;
    }
    else if (password == '') {
        document.getElementById('pwd').innerHTML = "Please choose any Password";
        return false;
    }
    else if (cpassword == '') {
        document.getElementById('cpwd').innerHTML = "Please Confirm Your Password";
        return false;
    }
    else if (!passexpr.test(password)) {
        document.getElementById('pwd').innerHTML = "Use uppercase, lowercase, special characters and numbers in this field";
        return false;
    }
    else if (password != cpassword) {
        document.getElementById('cpwd').innerHTML = "Password not matched";
        return false;
    }
    else if (mobile == '') {
        document.getElementById('mb').innerHTML = "Please enter mobile number";
        return false;
    }
    else if (!mob.test(mobile)) {
        document.getElementById('mb').innerHTML = "follow the pattern 123-456-7891";
        return false;
    }
    else if (address == '') {
        document.getElementById('add').innerHTML = "Please enter your address";
        return false;
    }
    else if (!add.test(address)) {
        document.getElementById('add').innerHTML = "Enter complete address";
        return false;
    }
    else if (document.getElementById('exampleInputPassword1').value.length < 6) {
        document.getElementById('pwd').innerHTML = "Password minimum length is 6";
        return false;
    }
    else if (document.getElementById('exampleInputPassword1').value.length > 12) {
        document.getElementById('pwd').innerHTML = "Max length reached";
        return false;
    }

    else {
        alert('Submission Done!!!');
        //Console.Log('Successful');
    }

}

function addRow(){

    var firstname = document.getElementById('frstnm').value;
    var middlename = document.getElementById('mddlnm').value;
    var lastname = document.getElementById('lstnm').value;
    var email = document.getElementById('emails').value;
    var mobile = document.getElementById('mobilenumber').value;
    var address = document.getElementById('exampleFormControlTextarea1').value;
    var qual = document.getElementById('Education').value;
    var gender = document.getElementById('gen').value;

    var table = document.getElementById('newtable');

    var newRow = table.insertRow(1);

    var cel1 = newRow.insertCell(0);
    var cel2 = newRow.insertCell(1);
    var cel3 = newRow.insertCell(2);
    var cel4 = newRow.insertCell(3);
    var cel5 = newRow.insertCell(4);
    var cel6 = newRow.insertCell(5);
    var cel7 = newRow.insertCell(6);
    var cel8 = newRow.insertCell(7);

    cel1.innerHTML = firstname;
    cel2.innerHTML = middlename;
    cel3.innerHTML = lastname;
    cel4.innerHTML = email;
    cel5.innerHTML = mobile;
    cel6.innerHTML = address;
    cel7.innerHTML = qual;
    cel8.innerHTML = gender;

    //var arr = [[firstname]]
    selectedrRowToInput();
}
var rIndex;

function selectedrRowToInput(){
    var tb = document.getElementById('newtable');
    //console.log(tb.row.length);
    for(var i=1; i < tb.rows.length-1; i++){
        
        tb.rows[i].onclick = function() {
            //get the selected row index
            
            rIndex = this.rowIndex;
            console.log(rIndex);

            document.getElementById('frstnm').value = this.cells[0].innerHTML;
            document.getElementById('mddlnm').value = this.cells[1].innerHTML;
            document.getElementById('lstnm').value = this.cells[2].innerHTML;
            document.getElementById('emails').value = this.cells[3].innerHTML;
            document.getElementById('mobilenumber').value = this.cells[4].innerHTML;
            document.getElementById('exampleFormControlTextarea1').value = this.cells[5].innerHTML;
            document.getElementById('Education').value = this.cells[6].innerHTML;
            document.getElementById('gen').value = this.cells[7].innerHTML;

        }
    }
}
//selectedrRowToInput();

function editHtmlTableSelectedRow(){
    var table = document.getElementById('newtable');

    var firstname = document.getElementById('frstnm').value;
    var middlename = document.getElementById('mddlnm').value;
    var lastname = document.getElementById('lstnm').value;
    var email = document.getElementById('emails').value;
    var mobile = document.getElementById('mobilenumber').value;
    var address = document.getElementById('exampleFormControlTextarea1').value;
    var qual = document.getElementById('Education').value;
    var gender = document.getElementById('gen').value;
    
    table.rows[rIndex].cells[0].innerHTML = firstname;
    table.rows[rIndex].cells[1].innerHTML = middlename;
    table.rows[rIndex].cells[2].innerHTML = lastname;
    table.rows[rIndex].cells[3].innerHTML = email;
    table.rows[rIndex].cells[4].innerHTML = mobile;
    table.rows[rIndex].cells[5].innerHTML = address;
    table.rows[rIndex].cells[6].innerHTML = qual;
    table.rows[rIndex].cells[7].innerHTML = gender;
}

function removeSelectedRow() {
    var table = document.getElementById('newtable');
    table.deleteRow(rIndex);
    document.getElementById('frstnm').value = "";
    document.getElementById('mddlnm').value = "";
    document.getElementById('lstnm').value = "";
    document.getElementById('emails').value = "";
    document.getElementById('mobilenumber').value = "";
    document.getElementById('exampleFormControlTextarea1').value = "";
    document.getElementById('Education').value = "";
    document.getElementById('gen').value = "";
}
﻿using System;

namespace TrainingTask5
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Bike b = new Bike("KTM", "Blue", 2, 180);
            b.Colour = "Black";
            b.start();
            b.speedup(50);
            int toll;
            b.CalToll(out toll);
            Console.WriteLine("Toll amount is:{0}", toll);
            b.speedup(60);
            b.stop();
        }
    }

    public abstract class Vehicle : IPainter
    {
        abstract public void CalToll(out int toll);
        private string Name;
        public int speed = 0;
        public string Colour;
        enum Noofwheels {
            Bike = 2,
            Car = 4,
        }

        readonly int Maxspeed = 100;
        public Vehicle(string Name, string colour, int Noofwheels, int Maxspeed)
        {
            this.Name = Name;
            this.Colour = colour;
            this.Maxspeed = Maxspeed;
        }
        public void start()
        {
            Console.WriteLine("The vehicle has started");
        }
        public void stop()
        {
            Console.WriteLine("The vehicle has stopped");
        }
        public void speedup(int speed)
        {
            this.speed += speed;
            if (this.speed < Maxspeed)
            {
                Console.WriteLine("Current speed is {0} kmph", this.speed);
            }
            else
            {
                Console.WriteLine("The speedup will go over the max speed(100kmph) please choose a lower value!!");
            }
        }

        public void paint()
        {
            Console.WriteLine("The new Color of vehicle is now Matt Black");
        }
    }
    public class Bike : Vehicle,SeatChanger
    {
        public Bike(string Name, string colour, int Noofwheels, int Maxspeed) : base(Name, colour, Noofwheels, Maxspeed)
        {

        }
        public override void CalToll(out int toll) {
            toll = 50 * 2;
        }

        public void change()
        {
            Console.WriteLine("The Seat Color have been changed to Quirky Red);
        }
    }
    sealed class Car : Vehicle,SeatChanger
    {
        public Car(string Name, string colour, int Noofwheels, int Maxspeed) : base(Name, colour, Noofwheels, Maxspeed)
        {

        }
        public override void CalToll(out int toll)
        {
            toll = 100 * 4;
        }
        public void change()
        {
            Console.WriteLine("The seat color has beeen changed to Playful Blue");
        }
    }

    interface IPainter
    {
        void paint();
    }

    interface SeatChanger
    {
         void change();
    }
}
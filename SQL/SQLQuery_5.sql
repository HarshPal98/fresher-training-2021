CREATE TABLE WorkerForce
(
    EmpId INT IDENTITY(104,1) PRIMARY KEY,
    Name VARCHAR(50) NOT NULL,
    Salary int NOT NULL
)

SELECT * FROM WorkerForce;

INSERT INTO WorkerForce (Name, Salary) VALUES ('Vineet', 1000), ('John', 1500), ('Suresh', 1100), ('Rick', 1300), ('Tom', 1400);

CREATE TABLE EmployeeAudit  
(  
Emp_ID int NOT NULL,  
Emp_name varchar(100) NOT NULL,  
Emp_Sal int NOT NULL,  
Audit_Action varchar(100),  
Audit_Timestamp datetime  
) 

SELECT * FROM EmployeeAudit;

ALTER TRIGGER trigger1 ON [dbo].[WorkerForce]
FOR INSERT
AS 
BEGIN
DECLARE @empid INT;
DECLARE @empname VARCHAR(100);  
DECLARE @empsal INT;  
DECLARE @auditaction VARCHAR(100); 
SELECT @empid=i.EmpId FROM inserted i;   
SELECT @empname=i.Name FROM inserted i;   
SELECT @empsal=i.Salary FROM inserted i;   
SET @auditaction='Inserted Record -- After Insert Trigger.'; 

INSERT INTO EmployeeAudit  (Emp_ID,Emp_Name,Emp_Sal,Audit_Action,Audit_Timestamp) VALUES (@empid,@empname,@empsal,@auditaction,getdate());  

PRINT 'AFTER INSERT trigger fired.'
END
GO

INSERT INTO WorkerForce VALUES('Ravi',1500); 

SELECT * FROM EmployeeAudit;

ALTER TRIGGER trigger2 ON [dbo].[WorkerForce]
AFTER UPDATE
AS
BEGIN
DECLARE @empid INT;
DECLARE @empname VARCHAR(100);  
DECLARE @empsal INT;  
DECLARE @auditaction VARCHAR(100); 
SELECT @empid=i.EmpId FROM inserted i;   
SELECT @empname=i.Name FROM inserted i;   
SELECT @empsal=i.Salary FROM inserted i;   
SET @auditaction='Inserted Record -- After Insert Trigger.'; 

INSERT INTO EmployeeAudit  (Emp_ID,Emp_Name,Emp_Sal,Audit_Action,Audit_Timestamp) VALUES (@empid,@empname,@empsal,@auditaction,getdate()); 
PRINT 'Updated Record -- After trigger';
END
GO

UPDATE WorkerForce SET Salary = 2100 WHERE Name = 'John';
SELECT * FROM EmployeeAudit;

CREATE TRIGGER trigger3 ON [dbo].[WorkerForce]
BEFORE DELETE
AS 
BEGIN
PRINT 'Deleted Record -- After Trigger';
END 
GO

DELETE FROM WorkerForce WHERE NAME='Vineet';

CREATE TABLE Test(
 custId INT NOT NULL PRIMARY KEY,
 yearJoined INT NOT NULL
)

INSERT INTO Test (custId, yearJoined) VALUES (1,2011), (2,2008), (3,2008), (4,2010), (5,2007), (6,2007), (7,2007), (8,2011);

SELECT * FROM Test;

SELECT yearJoined FROM Test GROUP BY yearJoined;

SELECT yearJoined FROM Test GROUP BY yearJoined HAVING yearJoined > 2008;

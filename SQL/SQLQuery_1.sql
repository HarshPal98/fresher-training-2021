CREATE DATABASE Task1;

CREATE TABLE Employee(
    EmpId int IDENTITY(101,1) PRIMARY KEY,
    EmpName VARCHAR(50) NOT NULL,
    Department VARCHAR(50) NOT NULL,
    ContactNo int NOT NULL,
    EmailId VARCHAR(50) NOT NULL,
    EmpHeadId int NOT NULL
)

select * from Employee;

INSERT INTO Employee (EmpName, Department, ContactNo, EmailId, EmpHeadId) VALUES ('Isha','E-101',1234567890,'isha@gmail.com',105);
INSERT INTO Employee (EmpName, Department, ContactNo, EmailId, EmpHeadId) VALUES ('Priya','E-104',1234567890,'priya@yahoo.com',103);
INSERT INTO Employee (EmpName, Department, ContactNo, EmailId, EmpHeadId) VALUES ('Neha','E-101',1234567890,'neha@gmail.com',101);
INSERT INTO Employee (EmpName, Department, ContactNo, EmailId, EmpHeadId) VALUES ('Rahul','E-102',1234567890,'rahul@yahoo.com',105);
INSERT INTO Employee (EmpName, Department, ContactNo, EmailId, EmpHeadId) VALUES ('Abhishek','E-101',1234567890,'abhishek@gmail.com',102);

CREATE TABLE EmpDept(
    DeptId VARCHAR(10) PRIMARY KEY,
    DeptName VARCHAR(50) NOT NULL,
    Dept_off varchar(50) NOT NULL,
    DeptHead int NOT NULL
)

select * from EmpDept;

INSERT INTO EmpDept (DeptId, DeptName, Dept_off, DeptHead) VALUES ('E-101', 'HR', 'MONDAY', 105);
INSERT INTO EmpDept (DeptId, DeptName, Dept_off, DeptHead) VALUES ('E-102', 'Development', 'TUESDAY', 101);
INSERT INTO EmpDept (DeptId, DeptName, Dept_off, DeptHead) VALUES ('E-103', 'House-Keeping', 'SATURDAY', 103);
INSERT INTO EmpDept (DeptId, DeptName, Dept_off, DeptHead) VALUES ('E-104', 'Sales', 'SUNDAY', 104);
INSERT INTO EmpDept (DeptId, DeptName, Dept_off, DeptHead) VALUES ('E-105', 'Purchase', 'TUESDAY', 104);

CREATE TABLE EmpSalary(
    EmpId int IDENTITY(101,1) PRIMARY KEY,
    Salary int NOT NULL,
    IsPermanent varchar(10) NOT NULL
)

select * from EmpSalary;

INSERT INTO EmpSalary (Salary, IsPermanent) VALUES (2000, 'Yes');
INSERT INTO EmpSalary (Salary, IsPermanent) VALUES (10000, 'Yes');
INSERT INTO EmpSalary (Salary, IsPermanent) VALUES (5000, 'No');
INSERT INTO EmpSalary (Salary, IsPermanent) VALUES (1900, 'Yes');
INSERT INTO EmpSalary (Salary, IsPermanent) VALUES (2300, 'Yes');

CREATE TABLE Project
(
    ProjectId VARCHAR(10) PRIMARY KEY,
    Duration int NOT NULL
)

select * from Project;

INSERT INTO Project (ProjectId, Duration) VALUES ('p-1', 23);
INSERT INTO Project (ProjectId, Duration) VALUES ('p-2', 15);
INSERT INTO Project (ProjectId, Duration) VALUES ('p-3', 45);
INSERT INTO Project (ProjectId, Duration) VALUES ('p-4', 2);
INSERT INTO Project (ProjectId, Duration) VALUES ('p-5', 30);

CREATE TABLE Country
(
    cid VARCHAR(10) PRIMARY KEY,
    cname VARCHAR(50) NOT NULL
)

select * from Country;

INSERT INTO Country (cid, cname) VALUES ('c-1', 'India');
INSERT INTO Country (cid, cname) VALUES ('c-2', 'USA');
INSERT INTO Country (cid, cname) VALUES ('c-3', 'China');
INSERT INTO Country (cid, cname) VALUES ('c-4', 'Pakistan');
INSERT INTO Country (cid, cname) VALUES ('c-5', 'Russia');

CREATE TABLE ClientTable
(
    ClientId VARCHAR(10) PRIMARY KEY,
    ClientName VARCHAR(50) NOT NULL,
    cid  VARCHAR(10) NOT NULL
)

select * from ClientTable;

INSERT INTO ClientTable (ClientId, ClientName, cid) VALUES ('c1-1', 'ABC Group', 'c-1');
INSERT INTO ClientTable (ClientId, ClientName, cid) VALUES ('c1-2', 'PQR', 'c-1');
INSERT INTO ClientTable (ClientId, ClientName, cid) VALUES ('c1-3', 'XYZ', 'c-2');
INSERT INTO ClientTable (ClientId, ClientName, cid) VALUES ('c1-4', 'tech altum', 'c-3');
INSERT INTO ClientTable (ClientId, ClientName, cid) VALUES ('c1-5', 'mnp', 'c-5');

CREATE TABLE EmpProject
(
    EmpId int IDENTITY(101,1) PRIMARY KEY,
    ProjectId VARCHAR(10) NOT NULL,
    ClientID VARCHAR(10) NOT NULL,
    StartYear int NOT NULL,
    EndYear int
)

select * from EmpProject;

INSERT INTO EmpProject (ProjectId, ClientID, StartYear, EndYear) VALUES ('p-1','C1-1',2010,2010);
INSERT INTO EmpProject (ProjectId, ClientID, StartYear, EndYear) VALUES ('p-2','C1-2',2010,2012);
INSERT INTO EmpProject (ProjectId, ClientID, StartYear) VALUES ('p-1','C1-3',2013);
INSERT INTO EmpProject (ProjectId, ClientID, StartYear, EndYear) VALUES ('p-4','C1-1',2014,2010);
INSERT INTO EmpProject (ProjectId, ClientID, StartYear) VALUES ('p-4','C1-5',2015);

select * FROM Employee WHERE EmpName LIKE 'P%';

select * FROM Employee WHERE EmailId LIKE '%@gmail%';

select * from Employee where Department='E-102' or Department='E-104';

select count(Salary) as count from EmpSalary where ispermanent='yes' and Salary>5000;

select DeptName from EmpDept where DeptId ='E-102';

SELECT EmpName FROM Employee AS EmployeeTable;
SELECT EmpName AS EmployeeTable FROM Employee;
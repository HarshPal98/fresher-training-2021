CREATE TABLE Student
(
   SystemId int IDENTITY(101,1) PRIMARY KEY,
   RollNo int NOT NULL,
   Name varchar(50) NOT NULL,
)

SELECT * FROM Student;
SELECT * FROM Info;

INSERT INTO Student(RollNo, Name) VALUES (1001, 'abc'), (1003, 'def'), (1005, 'xyz'), (1007, 'ghi'), (1009, 'jkl');

CREATE TABLE Info
(
    sid VARCHAR(10) PRIMARY KEY,
    RegNo int NOT NULL
)

INSERT INTO Info(sid, RegNo) VALUES ('a',1701), ('d',1702), ('x',1703), ('g',1704), ('j',1705);

SELECT * FROM Info;

SELECT CONCAT(SystemId, Name) FROM Student;

SELECT CHARINDEX('p', 'applause');

SELECT DATALENGTH('longestresultantstring');

SELECT SUBSTRING('GodzillavsKong', 4, 8);

SELECT UPPER(Name) FROM Student;

SELECT MIN(RollNo) FROM Student;

SELECT DATEDIFF(DAY,2021-05-27, 2021-05-24);

SELECT CURRENT_TIMESTAMP;

ALTER TABLE Student ADD marks INT;
UPDATE Student SET marks = 28 WHERE SystemId = 101;
UPDATE Student SET marks = 32 WHERE SystemId = 102;
UPDATE Student SET marks = 36 WHERE SystemId = 103;
UPDATE Student SET marks = 42 WHERE SystemId = 104;
UPDATE Student SET marks = 49 WHERE SystemId = 105;

CREATE TABLE #Student
(
   SystemId int IDENTITY(101,1) PRIMARY KEY,
   RollNo int NOT NULL,
   Name varchar(50) NOT NULL,
)
INSERT INTO #Student(RollNo, Name) VALUES (1001, 'abc'), (1003, 'def'), (1005, 'xyz'), (1007, 'ghi'), (1009, 'jkl');

SELECT Name As stuName from #Student;
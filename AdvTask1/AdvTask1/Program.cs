﻿using System;

namespace AdvTask1
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Welcome to the Successive Calculator");
            Console.WriteLine("Please select the action you want to perform");
            Console.WriteLine("Press 1 for Addition");
            Console.WriteLine("Press 2 for Subtraction");
            Console.WriteLine("Press 3 for Multiplication");
            Console.WriteLine("Press 4 for Division");
            Console.WriteLine("please enter your action");
            int choice = 0;
            try
            {
                choice = Convert.ToInt32(Console.ReadLine());
            }catch(Exception e)
            {
                Console.WriteLine(e);
            }
            
            int result = 0;
            switch (choice)
            {
                case 1:
                    Console.WriteLine("Enter the first value");
                    int n1 = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Enter the second value");
                    int n2 = Convert.ToInt32(Console.ReadLine());
                    try
                    {
                        result = Multiplication(n1, n2);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                    break;
                case 2:
                    Console.WriteLine("Enter the first value");
                    int in1 = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Enter the second value");
                    int in2 = Convert.ToInt32(Console.ReadLine());
                    try
                    {
                        result = Multiplication(in1, in2);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                    break;
                case 3:
                    Console.WriteLine("Enter the first value");
                    int inp1 = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Enter the second value");
                    int inp2 = Convert.ToInt32(Console.ReadLine());
                    try
                    {
                        result = Multiplication(inp1, inp2);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                    break;
                case 4:
                    Console.WriteLine("Enter the first value");
                    int input1 = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Enter the second value");
                    int input2 = Convert.ToInt32(Console.ReadLine());
                    if(input2 == 0)
                    {
                        Console.WriteLine("Divide by 0 exception");
                    }
                    else {
                        try
                        {
                            result = Multiplication(input1, input2);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e);
                        }
                    }  
                    break;
                default:Console.WriteLine("Wrong Entry. Please try again");
                    break;
            }
            Console.WriteLine("Result is {0}", result);
            Console.ReadKey();
        }

        public static int Addition(int input_1, int input_2)
        {
            int result = input_1 + input_2;
            return result;
        }
        //Substraction  
        public static int Subtraction(int input_1, int input_2)
        {
            int result = input_1 + input_2;
            return result;
        }
        //Multiplication  
        public static int Multiplication(int input_1, int input_2)
        {
            int result = input_1 + input_2;
            return result;
        }
        //Division  
        public static int Division(int input_1, int input_2)
        {
            int result = input_1 + input_2;
            return result;
        }
    }
}

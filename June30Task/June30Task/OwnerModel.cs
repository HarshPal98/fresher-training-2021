﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace June30Task.Models
{
    public class OwnerModel
    {
        public string Name { get; set; }
        public int marks { get; set; }
        public int Systemid { get; set; }
    }
}

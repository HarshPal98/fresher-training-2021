using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using June30Task.Models;
using System.Net;

namespace June30Task.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OwnerController : ControllerBase
    {
        private List<OwnerModel> stuList = new List<OwnerModel>();

        public OwnerController()
        {
            stuList.Add(new OwnerModel
            {
                Name = "Harsh",
                Systemid = 2017004108,
                marks = 94
            });

            stuList.Add(new OwnerModel
            {
                Name = "Mark",
                Systemid = 2017004110,
                marks = 60
            });

            stuList.Add(new OwnerModel
            {
                Name = "Tom",
                Systemid = 2017004112,
                marks = 97
            });

            stuList.Add(new OwnerModel
            {
                Name = "Joseph",
                Systemid = 2017004114,
                marks = 76
            });
        }

        [HttpGet]
        [ProducesResponseType(typeof(OwnerModel), (int)HttpStatusCode.OK)]
        public Task<OwnerModel> GetOwner()
        {
            var test = new OwnerModel
            {
                Name = "newuser",
                marks = 50,
                Systemid = 2017005110
            };
            return Task.FromResult(test);
        }

        [HttpGet("{id:int)")]
        [ProducesResponseType(typeof(OwnerModel), (int)HttpStatusCode.OK)]
        public Task<OwnerModel> GetOwner(int id)
        {
            var data = stuList.Find(f => f.Systemid == id);
            return Task.FromResult(data);
        }

        [HttpGet]
        [Route("GetAll")]
        [ProducesResponseType(typeof(IEnumerable<OwnerModel>), (int)HttpStatusCode.OK)]
        public Task<IEnumerable<OwnerModel>> GetAllOwner()
        {
            return Task.FromResult((IEnumerable<OwnerModel>)stuList);
        }

        [HttpPost]
        public Task<IEnumerable<OwnerModel>> InsertEmploye(OwnerModel om)
        {
            return Task.FromResult((IEnumerable<OwnerModel>)stuList);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Session30support.Models.Owner;
using Session30support.Models;
using Newtonsoft.Json;

namespace Session30support.Areas.Owner.Controllers
{
    [Area("Owner")]
    public class OwnerController : Controller
    {
        private readonly ILogger _logger;
        public OwnerController(ILogger<OwnerController> logger)
        {
            _logger = logger;
        }

        //GET: OwnerController
        public ActionResult Index()
        {
            return View("Owner", new OwnerViewModel());
        }

        public async Task<ActionResult> OwnerAsync(OwnerViewModel owner)
        {
            string baseURL = "";
            List<OwnerViewModel> obj = new List<OwnerViewModel>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseURL);
                var request = await client.GetAsync("api/Owner");

                if (request.IsSuccessStatusCode)
                {
                    var result = request.Content.ReadAsStringAsync().Result;
                    owner = JsonConvert.DeserializeObject<OwnerViewModel>(result);
                }
            }
            return View("Owner", owner);
        }
    }
}

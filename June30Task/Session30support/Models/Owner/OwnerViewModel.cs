﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Session30support.Models.Owner
{
    public class OwnerViewModel
    {
        public string Name { get; set; }
        public int marks { get; set; }
        public int Systemid { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace June29Task.Models
{
    public interface IEmployeeRepository
    {
        Employee GetEmployee(int Id);
    }
}

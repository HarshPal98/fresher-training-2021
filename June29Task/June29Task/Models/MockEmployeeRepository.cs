﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace June29Task.Models
{
    public class MockEmployeeRepository : IEmployeeRepository
    {
        private List<Employee> _employeeList;

        public MockEmployeeRepository()
        {
            _employeeList = new List<Employee>()
            {
                new Employee() {Id = 1, Name = "Harsh", Department = "DotNet", Email="harsh@gmail.com" },
                new Employee() {Id = 2, Name = "Sam", Department = "HR", Email="sam@gmail.com" },
                new Employee() {Id = 3, Name = "Tom", Department = "IT", Email="tomh@gmail.com" },
            };
        }

        public Employee GetEmployee(int Id)
        {
            return _employeeList.FirstOrDefault(e => e.Id == Id);
        }
    }
}

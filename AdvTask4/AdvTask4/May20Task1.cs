﻿using System;

namespace AdvTask4
{
    class StudentAndTeacherTes
    {
        public static void Main(string[] args)
        {
            Person p = new Person();
            p.Greet();
            p.SetAge(21);
            Student s = new Student();
            s.SetAge(21);
            s.Greet();
            s.ShowAge();
            Teacher t = new Teacher();
            t.SetAge(30);
            t.Greet();
            t.Explain();
        }
    }

    class Person
    {
        public int age;
        public void Greet()
        {
            Console.WriteLine("Hello");
        }
        public void SetAge(int n)
        {
            age = n;
        }
    }

    class Student : Person
    {
        public void GoToClasses()
        {
            Console.WriteLine("I’m going to class.");
        }

        public void ShowAge()
        {
            Console.WriteLine("My age is: {0}", age, "years old");
        }
    }

    class Teacher : Person
    {
        private string subject;
        public void Explain()
        {
            Console.WriteLine("Explanation begins");
        }
    }
}

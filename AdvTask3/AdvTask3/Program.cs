﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdvTask3
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            NationalAnthem indianAnthem = new NationalAnthem("India", "Jana Gana Mana", "Rabindranath Tagore", 1911);
        }
    }

    public class MusicalComposition
    {
        string title, composer;
        int year;

        public MusicalComposition(string title, string composer, int year)
        {
            this.title = title;
            this.composer = composer;
            this.year = year;
            Console.WriteLine("Please find the information below: ");
            Console.WriteLine("Title:{0}\n" + "Composer:{1}\n" + "Year:{2}\n", title, composer, year);
        }
    }

    public class NationalAnthem : MusicalComposition
    {
        string name;
        public NationalAnthem(string name, string Title, string Composer, int year) : base(Title, Composer, year)
        {
            this.name = name;
            Console.WriteLine("Nation Name is: "+ name);
        }
    }
}

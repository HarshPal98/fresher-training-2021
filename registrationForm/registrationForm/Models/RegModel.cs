﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace registrationForm.Models
{
    public class RegModel
    {
        [Required]
        [DataType(DataType.Text)]
        [DisplayName("Name")]
        public string FirstName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [DisplayName("Password")]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [DisplayName("Confirm Password")]
        public string CPassword { get; set; }

        [Required]
        [DataType(DataType.PhoneNumber)]
        [DisplayName("Mobile")]
        public long MobileNo { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [DisplayName("Email")]
        public string EmailId { get; set; }

        [Required]
        [DataType(DataType.MultilineText)]
        [DisplayName("Address")]
        public string Address { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [DisplayName("Gender")]
        public string Gender { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [DisplayName("Qualification")]
        public string Education { get; set; }
    }
}

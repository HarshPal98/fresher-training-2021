﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace registrationForm.Models
{
    public class Custom
    {

        [DataType(DataType.Text)]
        public string Name { get; set; }

        [DataType(DataType.PhoneNumber)]
        public long MobileNo { get; set; }

        [DataType(DataType.EmailAddress)]
        public string EmailId { get; set; }

        [DataType(DataType.MultilineText)]
        public string Address { get; set; }

        [DataType(DataType.Text)]
        public string Gender { get; set; }

        [DataType(DataType.Text)]
        public string Education { get; set; }
    }
}

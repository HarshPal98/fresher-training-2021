﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using registrationForm.Models;

namespace registrationForm.Controllers
{
    public class CustomController : Controller
    {
        [HttpPost]
        public ActionResult Details(RegModel model)
        {
            var name = model.FirstName;
            var email = model.EmailId;
            var mobile = model.MobileNo;
            var adrs = model.Address;
            var gend = model.Gender;
            var qual = model.Education;
            Custom c = new Custom { Name = name, EmailId = email, MobileNo = mobile, Address = adrs, Gender = gend, Education = qual };
            ViewBag.TableItems = new List<Custom> { c };
            return View(c);
        }
    }
}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SimpleCalculator.Models;

namespace SimpleCalculator.Controllers
{
    public class CalculatorController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            return View(new Calculator());
        }

        [HttpPost]
        public ActionResult Index(Calculator c, string calculate)
        {
            if (calculate == "add")
            {
                c.result = c.Value1 + c.Value2;
            }
            else if (calculate == "min")
            {
                c.result = c.Value1 - c.Value2;
            }
            else if (calculate == "mul")
            {
                c.result = c.Value1 * c.Value2;
            }
            else
            {
                c.result = c.Value1 / c.Value2;
            }

            return View("Index", c);
        }
    }
}

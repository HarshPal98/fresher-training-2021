﻿using System;

namespace SimpleCalculator.Models
{
    public class Calculator
    {
        public int Value1 { get; set; }

        public int Value2 { get; set; }

        public int result { get; set; }
    }
}

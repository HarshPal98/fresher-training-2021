using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using June28Task.Models;
using June28Task.Areas.Employee.Filters;

namespace June28Task.Areas.Employee.Controllers
{
    [Area("Employee")]
    public class EmployeeController : Controller
    {
        [ServiceFilter(typeof(CustomEmployeeFilter))]
        public IActionResult Index()
        {
            return View(new EmployeeViewModel());
        }

        public ActionResult Index(EmployeeViewModel evm, string sub)
        {
            if(sub == "Save")
            {
                
            }
            return View("Index", evm);
        }
    }
}
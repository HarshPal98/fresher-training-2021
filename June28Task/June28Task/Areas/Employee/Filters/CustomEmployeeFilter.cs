﻿using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Diagnostics;

namespace June28Task.Areas.Employee.Filters
{
    public class CustomEmployeeFilter : IActionFilter
    {
        private ILogger _logger;
        public CustomEmployeeFilter(ILoggerFactory loggerFactory)
        {
            _logger = loggerFactory.CreateLogger<CustomEmployeeFilter>();
        }
        public void OnActionExecuted(ActionExecutedContext context)
        {
            var routdata = context.RouteData;
            var action = routdata.Values["action"];
            var controller = routdata.Values["controller"];
            Debug.Write(string.Format("Controller: {0} Action: {1} ", controller, action), "Log from Action Filter OnActionExecuted: ");
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            var routdata = context.RouteData;
            var action = routdata.Values["action"];
            var controller = routdata.Values["controller"];
            Debug.Write(string.Format("Controller: {0} Action: {1} ", controller, action), "Log from Action Filter OnActionExecuting: ");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace June28Task.Areas.Employee.Models
{
    public class Employee
    {
        public string Name { get; set; }
        public int Salary { get; set; }
    }
}

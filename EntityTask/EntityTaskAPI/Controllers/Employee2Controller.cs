﻿using AutoMapper;
using EntityTaskAPI.Models;
using Data;
using Data.Model;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace EntityTaskAPI.Controllers
{
    [ApiVersion("2.0")]
    [ApiController]
    [Route("api/v{version:apiVersion}/Test")]
    public class Employee2Controller : Controller
    {
        private MyDataContext _myDataContext;
        private IMapper _mapper;

        public Employee2Controller(MyDataContext myDataContext, IMapper mapper)
        {
            _mapper = mapper;
            _myDataContext = myDataContext;
        }

        [HttpGet("{id:int}")]
        [ProducesResponseType(typeof(EmployeeModel), (int)HttpStatusCode.OK)]
        //[ProducesResponseType(typeof(DBNull), (int)HttpStatusCode.NotFound)]
        public Task<EmployeeModel> GetEmployee(int id)
        {
            var data = _mapper.Map<EmployeeModel>(_myDataContext.Employees.Where(f => f.EmployeeId == id).FirstOrDefault());
            //var allRecords = from emp in _myDataContext.Employees
            //                 select emp;

            return Task.FromResult(data);
        }


        [HttpGet]
        [Route("GetAll")]
        [ProducesResponseType(typeof(IEnumerable<EmployeeModel>), (int)HttpStatusCode.OK)]
        //[ProducesResponseType(typeof(DBNull), (int)HttpStatusCode.NotFound)]
        public Task<IEnumerable<EmployeeModel>> GetAllEmployee()
        {
            var data = _mapper.Map<IList<EmployeeModel>>(_myDataContext.Employees.ToList());

            return Task.FromResult((IEnumerable<EmployeeModel>)data);
        }

        [HttpPost]
        [Route("Save")]
        [ProducesResponseType(typeof(IEnumerable<EmployeeModel>), (int)HttpStatusCode.OK)]
        //[ProducesResponseType(typeof(DBNull), (int)HttpStatusCode.NotFound)]
        public Task<IEnumerable<EmployeeModel>> SaveEmployee(Employee employee)
        {
            _myDataContext.Employees.Add(employee);
            _myDataContext.SaveChanges();
            var data = _mapper.Map<IList<EmployeeModel>>(_myDataContext.Employees.ToList());
            return Task.FromResult((IEnumerable<EmployeeModel>)data);
        }
    }
}

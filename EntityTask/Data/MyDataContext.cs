﻿using Data.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public class MyDataContext : DbContext
    {
        public MyDataContext(DbContextOptions<MyDataContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<Employee>().Property(p => p.EmployeeName).HasMaxLength(50).IsRequired();
        }

        public DbSet<Employee> Employees { get; set; }

        public DbSet<EmailAddress> EmailAddresses { get; set; }

    }
}

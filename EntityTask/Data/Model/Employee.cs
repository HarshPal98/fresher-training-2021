﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Data.Model
{
    public class Employee
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int EmployeeId { get; set; }

        [Column(TypeName = "varchar(50)")]
        [Required]
        public string EmployeeName { get; set; }

        [Required]
        [Column(TypeName = "tinyint")]
        public int EmployeeAge { get; set; }

        public List<EmailAddress> EmailAddresses { get; set; }

    }
}

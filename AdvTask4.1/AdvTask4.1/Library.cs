﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdvTask4
{
    public class Library
    {
        public int Serial_No = 0;
        public struct Book
        {
            public string Title;
            public string Author;
        }

        Book[] Lib = new Book[100];

        public void AddBook(string title, string author)
        {
            Lib[Serial_No].Title = title;
            Lib[Serial_No].Author = author;
            Serial_No++;
        }

        public void SearchByTitle(String title)
        {
            for (int i = 0; i < Lib.Length; i++)
            {
                if (Lib[i].Title.Equals(title))
                {
                    Console.WriteLine("Title:" + Lib[i].Title + " Author:" + Lib[i].Author);
                }
                else
                {
                    Console.WriteLine("Book not found.");
                }
            }
        }

        public void DisplayBooks()
        {
            foreach(var b in Lib)
            {
                Console.WriteLine("Title : {0} & Author: {1}", b.Title, b.Author);
            }
        }

        public void DeleteBook(int index)
        {
            if (index < 1000)
            {
                Lib[index].Title = null;
                Lib[index].Author = null;
            }
        }

        public static void Main(String[] args)
        {
            Library lb = new Library();
            UserChoice:
            Console.WriteLine("Welcome to Library");
            Console.WriteLine("Press 1 to Add a Book");
            Console.WriteLine("Press 2 to Delete a Book");
            Console.WriteLine("Press 3 to Display Books");
            Console.WriteLine("Press 4 to Search Book");
            Console.WriteLine("Press 5 to exit");
            int ch = Convert.ToInt32(Console.ReadLine());

            switch (ch)
            {
                case 1:Console.WriteLine("Enter the title and author");
                    string title = Console.ReadLine();
                    string author = Console.ReadLine();
                    lb.AddBook(title, author);
                    break;
                case 2:Console.WriteLine("Enter the index");
                    int idx = Convert.ToInt32(Console.ReadLine());
                    lb.DeleteBook(idx);
                    break;
                case 3:
                    lb.DisplayBooks();
                    break;
                case 4:
                    Console.WriteLine("Enter the title of the book");
                    string titlee = Console.ReadLine();
                    lb.SearchByTitle(titlee);
                    break;
                case 5:Console.WriteLine("Exit");
                    break;
                default:
                    goto UserChoice;
            }
        }
    }
}

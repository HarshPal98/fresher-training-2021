﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdvTask4
{
    class House
    {
        Door door = new Door();
        HousePerson HP = new HousePerson();
        public int Area
        {
            get;
            set;
        }
        public virtual void ShowData()
        {
            Console.WriteLine("I am a house, my area is : " + Area + "\n" + "Door Colour:" + door.Colour + "\n" +
                              "Person Name:" + HP.Name);
        }
        void DoorColour()
        {
            Door door = new Door();
            Console.WriteLine("Enter door colour:");
            door.Colour = Console.ReadLine();
        }
        void People()
        {
            HousePerson HP = new HousePerson();
            Console.WriteLine("Enter door colour:");
            HP.Name = Console.ReadLine();
        }


    }
    class SmallApartment : House
    {
        int Area = 50;
        Door door = new Door();
        HousePerson HP = new HousePerson();
        public SmallApartment()
        {
            Console.WriteLine("Small Apartment Area :" + Area);
        }
        void DoorColour()
        {
            Console.WriteLine("Enter door colour:");
            door.Colour = Console.ReadLine();
        }
        void People()
        {
            Console.WriteLine("Enter door colour:");
            HP.Name = Console.ReadLine();
        }

    }
    public class Door
    {
        public string Colour
        {
            get;
            set;
        }
        public void ShowData()
        {
            Console.WriteLine("I am a door ,my colour is " + Colour);
        }
    }
    class HousePerson
    {

        public string Name
        {
            get;
            set;
        }
        public void ShowData()
        {
            Console.WriteLine("My name is:" + Name);
        }

    }

}
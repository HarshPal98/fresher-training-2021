﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace AdvTask2
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            //string choice = -1;
            Console.WriteLine("Welcome to the App");
            Console.WriteLine("Press 1 for Sphere");
            Console.WriteLine("Press 2 for Cylinder");
            Console.WriteLine("Press 3 for Cube");
            Console.WriteLine("Press 4 for Triangle");

        UserChoice: Console.WriteLine("Please enter your choice");
            string choice = Console.ReadLine();
            bool flag = false;
            int ch = 0;
            try
            {
                flag = Int32.TryParse(choice, out ch);
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
                goto UserChoice;
            }
            if (flag)
            {
                switch (ch)
                {
                    case 1:
                        Sphere sph = new Sphere();
                        Console.WriteLine("Enter the radius of the Sphere");
                        string rad = Console.ReadLine();
                        Console.WriteLine("Area:" + sph.Area(rad));
                        break;
                    case 2:
                        Cylinder cy = new Cylinder();
                        Console.WriteLine("Enter the radius of the cylinder");
                        string radius = Console.ReadLine();
                        Console.WriteLine("Enter the height of the cylinder");
                        string height = Console.ReadLine();
                        Console.WriteLine("Area:" + cy.Area(radius, height));
                        break;
                    case 3:
                        Cube cub = new Cube();
                        Console.WriteLine("Enter the side of the cube");
                        string side = Console.ReadLine();
                        Console.WriteLine("Area:" + cub.Area(side));
                        break;
                    case 4:
                        Triangle t = new Triangle();
                        Console.WriteLine("Enter the base of the triangle");
                        string bas = Console.ReadLine();
                        Console.WriteLine("Enter the height of the triangle");
                        string hei = Console.ReadLine();
                        Console.WriteLine("Area:" + t.Area(bas, hei));
                        break;
                    default:
                        Console.WriteLine("Incorrect Choice, please try again!");
                        goto UserChoice;
                }
            }
            else
            {
                Console.WriteLine("Please Entry Correctly");
            }
            Console.ReadKey();
        }
    }

    public abstract class Type1
    {
        abstract public double Area(string r);
    }

    public abstract class Type2
    {
        abstract public double Area(string r, string h);
    }

    class Cube : Type1
    {
        public override double Area(string side)
        {
            bool flag = false;
            double s = 0;
            try
            {
                flag = Double.TryParse(side, out s);
            }
            catch (Exception e)
            {
                Console.WriteLine("" + e.Message);
            }
            if (flag == true)
            {
                double Area = 6 * s * s;
                return Area;
            }
            else
            {
                Console.WriteLine("EWrong Entry!!");
            }

            return -1;
        }
    }

    class Sphere : Type1
    {
        const double PI = 3.1459265;
        public override double Area(string r)
        {
            bool flag = false;
            double rad = 0;
            try
            {
                flag = Double.TryParse(r, out rad);
            }
            catch (Exception e)
            {
                Console.WriteLine("" + e.Message);
            }
            if (flag == true)
            {
                double Area = 4 * PI * rad * rad;
                return Area;
            }
            else
            {
                Console.WriteLine("Wrong Entry!!");
            }
            return -1;
        }
    }
    class Cylinder : Type2
    {
        const double PI = 3.1459265;
        public override double Area(string r, string h)
        {
            bool flag1 = false, flag2 = false;
            double rad = 0, ht = 0;
            try
            {
                flag1 = Double.TryParse(r, out rad);
                flag2 = Double.TryParse(h, out ht);
            }
            catch (Exception e)
            {
                Console.WriteLine("" + e);
            }
            if (flag1 == true && flag2 == true)
            {
                double Area = 2 * PI * rad * (rad + ht);
                return Area;
            }
            else
            {
                Console.WriteLine("Wrong Entry!!");
            }

            return -1;
        }
    }

    class Triangle : Type2
    {
        public override double Area(string b, string h)
        {
            bool flag1 = false, flag2 = false;
            double r = 0, ht = 0;
            try
            {
                flag1 = Double.TryParse(b, out r);
                flag2 = Double.TryParse(h, out ht);
            }
            catch (Exception e)
            {
                Console.WriteLine("" + e);
            }
            if (flag1 == true && flag2 == true)
            {
                double Area = 0.5 * r * ht;
                return Area;
            }
            else
            {
                Console.WriteLine("Wrong Entry!!");
            }

            return -1;
        }
    }
}

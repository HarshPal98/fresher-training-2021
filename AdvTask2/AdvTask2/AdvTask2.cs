﻿using System;
using System.IO;

namespace AdvTask2
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            double r=0, side=0, h=0;
            int choice = -1;
            Console.WriteLine("Welcome to the App");
            Console.WriteLine("Press 1 for Sphere");
            Console.WriteLine("Press 2 for Cylinder");
            Console.WriteLine("Press 3 for Cube");

        UserChoice: Console.WriteLine("Please enter your choice");
            try
            {
                choice = Convert.ToInt32(Console.ReadLine());
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
            switch (choice)
            {
                case 1:
                    Console.WriteLine("Enter the Radius of the Sphere: ");
                    try
                    {
                        r = Convert.ToDouble(Console.ReadLine());
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    break;
                case 2:
                    try
                    {
                        Console.WriteLine("Enter the Radius of the Cylinder: ");
                        r = Convert.ToDouble(Console.ReadLine());
                        Console.WriteLine("Enter the height of the Cylinder: ");
                        h = Convert.ToDouble(Console.ReadLine());
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    break;
                case 3:
                    try
                    {
                        side = Convert.ToDouble(Console.ReadLine());
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    break;
                default:
                    Console.WriteLine("Incorrect Choice, please try again!");
                    goto UserChoice;
            }

            Shape Cub = new Cube();
            Shape Sph = new Sphere();
            Shape Cyl = new Cylinder();
            if (choice == 1)
            {
                Sph.SurfaceArea(r);
                Sph.Volume(r);
                Sph.ShowResult();
            }
            else if (choice == 2)
            {
                Cyl.SurfaceArea(r, h);
                Cyl.Volume(r, h);
                Cyl.ShowResult();
            }
            else
            {
                Cub.SurfaceArea(side);
                Cyl.Volume(side);
                Cub.ShowResult();
            }

            Console.ReadKey();
        }
    }
    class Shape
    {
        public double surface_area, volume;
        public virtual void SurfaceArea(double r)
        {
        }
        public virtual void SurfaceArea(double r, double h)
        {
        }
        public virtual void Volume(double r)
        {
        }
        public virtual void Volume(double r, double h)
        {
        }
        public void ShowResult()
        {
            Console.WriteLine($"Your Surface Are is {surface_area}");
            Console.WriteLine($"Your Volume is {volume}");
        }
    }

    class Cube : Shape
    {
        public override void SurfaceArea(double side)
        {
            surface_area = side * side * side;
        }

        public override void Volume(double side)
        {
            volume = side * side * side;
        }
    }

    class Sphere : Shape
    {
        public override void SurfaceArea(double r)
        {
            surface_area = 4 * 3.14159 * r * r;
        }

        public override void Volume(double r)
        {
            volume = (4.0 / 3) * 3.14159 * r * r * r;
        }
    }

    class Cylinder : Shape
    {
        public override void SurfaceArea(double r, double h)
        {
            surface_area = (22 * r * (r + h)) / 7;
        }

        public override void Volume(double r, double h)
        {
            volume = 3.14159 * r * r * h;
        }
    }
}

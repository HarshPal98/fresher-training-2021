﻿using System;

namespace AdvTrainingJun1
{
    class MainClass
    {
        public delegate void DelegateFuncPtr(int a, int b);
        public delegate void NumberSwap(int n);
        public static void Main(string[] args)
        {
            Console.WriteLine("Welcome to Today's task");
            Console.WriteLine("Press 1 to run Delegate function");
            Console.WriteLine("Press 2 to run Anonymous function");
            Console.WriteLine("Press 3 to run Lambda function");
            Console.WriteLine("Enter your choice");
            int ch = Convert.ToInt32(Console.ReadLine());
            MainClass m = new MainClass();
            switch (ch)
            {
                case 1:
                    DelegateFuncPtr dobj = new DelegateFuncPtr(m.DelegateFunc);
                    dobj.Invoke(120, 60);
                    break;
                case 2:
                    NumberSwap nobj = delegate (int n)
                    {
                        Console.WriteLine("This is Anonymous Function and value is {0}", n);
                    };
                    nobj(20);
                    break;
                case 3:
                    DelegateFuncPtr sum = (int number1, int number2) =>
                    {
                        Console.WriteLine("This is the Lambda expression of th above delegate function");
                        Console.WriteLine($"{number1} + {number2} = {number1 + number2}");
                    };
                    sum(10, 5);
                    break;
                default:
                    Console.WriteLine("Wrong Entry");
                    break;
            }
        }

        public void DelegateFunc(int a, int b)
        {
            Console.WriteLine("Delegate Function is invoked");
            Console.WriteLine(a + b);
        }
    }
}
